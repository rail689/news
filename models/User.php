<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use amnah\yii2\user\models\User as BaseUser;
use yii\helpers\ArrayHelper;
use yii\swiftmailer\Mailer;
use yii\swiftmailer\Message;

class User extends BaseUser
{
    const ROLE_ADMIN = 'admin';
    const ROLE_MANAGER = 'manager';

    const SCENARIO_ADMIN = 'admin';

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['status', 'default', 'value' => static::STATUS_UNCONFIRMED_EMAIL];
        return $rules;
    }


    /**
     * @param bool $insert
     */
    public function afterSave($insert, $changedAttributes)
    {
        if (ArrayHelper::keyExists("role_id", $changedAttributes)) {
            $role_item_name = self::getRoleIdByAuthItemName($this->role_id);
            Yii::$app->authManager->revokeAll($this->id);
            if (!is_null($role_item_name)) {
                $role = Yii::$app->authManager->createRole($role_item_name);
                Yii::$app->authManager->assign($role, $this->id);
            }
        }
    }

    private static function getRoleIdByAuthItemName($role_id)
    {
        $map = [
            Role::ROLE_ADMIN => self::ROLE_ADMIN,
            Role::ROLE_MANAGER => self::ROLE_MANAGER,
        ];
        return ArrayHelper::getValue($map, $role_id);
    }

    public function sendEmailConfirmation($userToken)
    {
        /** @var Mailer $mailer */
        /** @var Message $message */

        // modify view path to module views
        $mailer = Yii::$app->mailer;
        $oldViewPath = $mailer->viewPath;
        $mailer->viewPath = $this->module->emailViewPath;

        // send email
        $user = $this;
        $profile = $user->profile;
        $email = $userToken->data ?: $user->email;
        $subject = Yii::$app->id . " - " . Yii::t("user", "Email Confirmation");
        $result = $mailer->compose('confirmEmail', compact("subject", "user", "profile", "userToken"))
            ->setTo($email)
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['brandName']])
            ->setSubject($subject)
            ->send();

        // restore view path and return result
        $mailer->viewPath = $oldViewPath;
        return $result;
    }
}
