<?php

namespace app\models\search;

use app\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\News;

/**
 * NewsSearch represents the model behind the search form of `app\models\News`.
 */
class NewsSearch extends News
{

    public $date_from;
    public $date_to;
    public $description;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'safe'],
            [['date_from', 'date_to'], 'date', 'format' => 'php:d-m-Y']
        ];
    }

    public function attributeLabels()
    {
        return [
            'date_from' => Yii::t('custom', 'Date from'),
            'date_to' => Yii::t('custom', 'Date to'),
            'description' => Yii::t('custom', 'Description'),
            'status' => Yii::t('custom', 'Status'),
            'name' => Yii::t('custom', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Yii::$app->session->get('per-page', 5),
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
             $query->where('0=1');
            return $dataProvider;
        }
        if (!Yii::$app->user->can(User::ROLE_MANAGER)) {
            $query->andWhere(['status' => News::STATUS_ACTIVE]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'status' => $this->status,
            'name' => $this->name,
        ]);

        $query->andFilterWhere(['like', 'full_text', $this->description])
            ->andFilterWhere(['like', 'short_text', $this->description]);

        if ($this->date_from) {
            $query->andFilterWhere([
                '>=',
                'created_at',
                $this->date_from . ' 00:00:00'
            ]);
        }
        if ($this->date_to) {
            $query->andFilterWhere([
                '<=',
                'created_at',
                $this->date_to . ' 23:59:59'
            ]);
        }

        return $dataProvider;
    }
}