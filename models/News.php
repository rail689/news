<?php

namespace app\models;

use amnah\yii2\user\models\Profile;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property int $status
 * @property string $full_text
 * @property string $short_text
 * @property string $picture_path
 * @property string $name
 * @property int $profile_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Profile $profile
 * @property UploadedFile $image
 */
class News extends \yii\db\ActiveRecord
{
    public $image;

    const PICTURE_UPLOAD_DIR = 'user/photo/';
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => function () {
                    return gmdate("Y-m-d H:i:s");
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'short_text', 'profile_id'], 'required'],
            [['status', 'profile_id'], 'integer'],
            [['full_text', 'short_text'], 'string'],
            [['image', 'created_at', 'updated_at'], 'safe'],
            [['picture_path', 'name'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'jpg, gif, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('custom', 'ID'),
            'status' => Yii::t('custom', 'Status'),
            'full_text' => Yii::t('custom', 'Full Text'),
            'short_text' => Yii::t('custom', 'Short Text'),
            'picture_path' => Yii::t('custom', 'Picture Path'),
            'name' => Yii::t('custom', 'Name'),
            'image' => Yii::t('custom', 'Image'),
            'profile_id' => Yii::t('custom', 'Profile ID'),
            'created_at' => Yii::t('custom', 'Created At'),
            'updated_at' => Yii::t('custom', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    public function namePicture()
    {
        $this->picture_path = Yii::$app->security->generateRandomString();

        return self::getPictureDest($this->picture_path);
    }

    public static function getPictureDest($name)
    {
        $base_path = Yii::getAlias('@app') . '/uploads/' . self::PICTURE_UPLOAD_DIR;
        if (!is_null($name)) {
            return $base_path . $name;
        } else {
            return $base_path . 'default';
        }
    }

    public static function getImage($name)
    {
        $file_path = self::getPictureDest($name);
        if (file_exists($file_path)) {
            return file_get_contents($file_path);
        }
        return null;
    }


    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('custom', 'Active'),
            self::STATUS_INACTIVE => Yii::t('custom', 'Deactive')
        ];
    }

    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function changeStatus($status)
    {
        if ($this->status == $status) {
            return true;
        } else {
            $this->status = $status;
            return $this->save();
        }
    }


}
