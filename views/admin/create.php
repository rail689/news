<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $message  string */

$this->title = Yii::t('user', 'Create {modelClass}', [
    'modelClass' => 'User',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span>
        </button>
        <div class="modal-title">
            <?= Html::encode($this->title) ?>
        </div>
    </div>
    <div class="modal-body">
        <?= $this->render('_form', [
            'user' => $user,
            'profile' => $profile,
        ]) ?>
    </div>
</div>