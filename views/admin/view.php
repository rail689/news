<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var amnah\yii2\user\models\User $user
 */

$this->title = $user->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('user', 'Update'),
            ['update', 'id' => $user->id],
            [
                'class' => 'btn btn-primary',
                'data-target' => '#remote_modal',
                'data-toggle' => 'modal'
            ]) ?>
        <?= Html::a(
            Yii::t('user', 'Delete'),
            ['delete', 'id' => $user->id],
            [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('user', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $user,
        'attributes' => [
            'id',
            'email:email',
            'username',
            'logged_in_at',
            'created_at',
        ],
    ]) ?>

</div>
