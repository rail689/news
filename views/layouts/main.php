<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\User;
use app\widgets\Alert;
use budyaga\users\components\AuthorizationWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->params['brandName'],
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    $items = [
        ['label' => Yii::t("custom", "Home"), 'url' => ['/site/index']]
    ];
    if (Yii::$app->user->can(User::ROLE_ADMIN)) {
        $items[] = ['label' => Yii::t("custom", "Users"), 'url' => ['/user/admin']];
    }
    if (Yii::$app->user->isGuest) {
        $items[] = ['label' => Yii::t("custom", "Login"), 'url' => ['/user/login']];
        $items[] = ['label' => Yii::t("custom", "Register"), 'url' => ['/user/register']];
    } else {
        $items[] = ['label' => Yii::t("custom", "Account"), 'url' => ['/user/account']];
        $items[] = ['label' => Yii::t("custom", "Profile"), 'url' => ['/user/profile']];
        $items[] =
            '<li>'
            . Html::beginForm(['/user/logout'], 'post')
            . Html::submitButton(
                Yii::t("custom", "Logout") . ' (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Yii::$app->params['brandName'] ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?= Modal::widget([
    'id' => 'remote_modal',
    'clientOptions' => false,
    'closeButton' => [
        'label' => '',
    ],
    'options' => [
        'class' => 'ajax-modal',
        'tabindex' => false,
    ]
]); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
