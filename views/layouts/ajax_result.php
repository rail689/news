<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $message  string */

?>
<div>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <p> <?= $message ?> </p>
    </div>
    <div class="modal-footer">
        <?= Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal']); ?>
    </div>
</div>
