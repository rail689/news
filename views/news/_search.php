<?php

use app\models\News;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php if (!Yii::$app->user->isGuest): ?>
        <?= $form->field($model, 'status')->dropDownList(News::getStatuses()) ?>
    <?php endif; ?>
    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'description') ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'date_from') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'date_to') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('custom', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('custom', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
