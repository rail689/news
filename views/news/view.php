<?php

use app\utils\PermissionHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('custom', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can(
        PermissionHelper::PERMISSION_EDIT_NEWS,
        ['news_id' => $model->id])
    ) : ?>
        <p>
            <?= Html::a(Yii::t('custom', 'Update'),
                ['update', 'id' => $model->id],
                [
                    'class' => 'btn btn-success',
                    'data-target' => '#remote_modal',
                    'data-toggle' => 'modal'
                ]) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php endif; ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'full_text:ntext',
            'short_text:ntext',
            'name',
            'created_at:datetime',
        ],
    ]) ?>

</div>
