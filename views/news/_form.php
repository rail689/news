<?php

use app\models\News;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
        'action' => Yii::$app->request->absoluteUrl,
        'options' => [
            'id' => 'news_create_form',
            'enctype' => 'multipart/form-data'
        ],
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validationUrl' => ['validate', 'id' => $model->id]
    ]); ?>

    <?= $form->field($model, 'status')->dropDownList(News::getStatuses()) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'full_text')->textarea(['rows' => 3]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'short_text')->textarea(['rows' => 3]) ?>
        </div>
    </div>

    <?= $form->field($model, 'image')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'allowedFileExtensions' => ['jpg', 'gif', 'png'],
                'showPreview' => false,
                'showCaption' => true,
                'showRemove' => false,
                'uploadAsync' => false,
                'dropZoneEnabled' => false,
                'showUpload' => false,
            ]
        ]
    ); ?>

    <div class="form-group">
        <?= Html::button(Yii::t('custom', 'Save'), ['class' => 'file-modal-submit btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
