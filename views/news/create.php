<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = Yii::t('custom', 'Create News');
$this->params['breadcrumbs'][] = ['label' => Yii::t('custom', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;</span>
        </button>
        <div class="modal-title">
            <?= Html::encode($this->title) ?>
        </div>
    </div>
    <div class="modal-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
