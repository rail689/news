<?php


/* @var $this yii\web\View */

use app\models\News;
use app\utils\PermissionHelper;
use yii\bootstrap\Html;

/* @var $model app\models\News */
?>

<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <?= $model->name ?>
        </div>
        <div class="panel-body">
            <?= Html::img(['/news/picture', 'name' => $model->picture_path],['class'=>'news-picture']) ?>
            <?php if (Yii::$app->user->isGuest) : ?>
                <?= $model->short_text ?>
            <?php else : ?>
                <?= $model->full_text ?>
            <?php endif; ?>

            <?php if (Yii::$app->user->can(
                PermissionHelper::PERMISSION_EDIT_NEWS,
                ['news_id' => $model->id])
            ) : ?>
                <?= Html::a('', ['/news/update', 'id' => $model->id],
                    [
                        'class' => 'glyphicon glyphicon-pencil',
                        'data-target' => '#remote_modal',
                        'data-toggle' => 'modal'
                    ]); ?>
                <?= Html::a('', ['/news/delete', 'id' => $model->id],
                    [
                        'class' => 'glyphicon glyphicon-trash',
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                    ]); ?>
                <?= Html::a('', ['/news/view', 'id' => $model->id],
                    [
                        'class' => 'glyphicon glyphicon-eye-open',
                    ]); ?>
                <?= Html::button(($model->isActive()) ? Yii::t('custom', 'Deactivate')
                    : Yii::t('custom', 'Activate'),
                    [
                        'data-id' => $model->id,
                        'data-status_id' => ($model->isActive()) ? News::STATUS_INACTIVE : News::STATUS_ACTIVE,
                        'class' => 'news-status btn btn-default',
                    ]); ?>
            <?php endif; ?>
        </div>
    </div>


</div>
