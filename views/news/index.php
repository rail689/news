<?php

use app\models\User;
use yii\bootstrap\BaseHtml;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('custom', 'News');
$this->params['breadcrumbs'][] = $this->title;
$view = $this;
$items = [];
for ($index = 0; $index < 20; $index += 5) {
    if ($index == 0) {
        $item_count = $index + 1;
    } else {
        $item_count = $index;
    }
    $items[] = [
        'label' => Yii::t('custom', 'Show') . ' ' . $item_count,
        'url' => ['/news/index', 'per-page' => $item_count]
    ];
}

?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="dropdown">
        <a href="#" data-toggle="dropdown"
           class="btn btn-default dropdown-toggle"><?= Yii::t('custom', 'Page item count') ?><b class="caret"></b></a>
        <?php
        echo Dropdown::widget([
            'items' => $items,
        ]);
        ?>
    </div>
    </br>

    <?php if (Yii::$app->user->can(User::ROLE_MANAGER)): ?>
        <p>
            <?= Html::a(
                Yii::t('custom', 'Create News'),
                ['create'],
                [
                    'class' => 'btn btn-success',
                    'data-target' => '#remote_modal',
                    'data-toggle' => 'modal'
                ]
            ) ?>
        </p>
    <?php endif; ?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) use ($view) {
            return $view->render('_item', ['model' => $model]);
        },
    ]) ?>
</div>
