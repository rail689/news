<?php
namespace app\utils;

use amnah\yii2\user\models\UserToken;

/**
 * Created by PhpStorm.
 * User: rail
 * Date: 29.08.17
 * Time: 22:24
 */
class EmailHelper
{

    /**
     * @param $user \app\models\User
     */
    public static function sendEmailConfirmation($user)
    {
        /** @var \amnah\yii2\user\models\UserToken $userToken */
        $userToken = new UserToken();

        $userTokenType = null;
        $userTokenType = $userToken::TYPE_EMAIL_CHANGE;

        // check if we have a userToken type to process, or just log user in directly
        $userToken = $userToken::generate($user->id, $userTokenType);
        $user->sendEmailConfirmation($userToken);
    }

}