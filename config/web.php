<?php

use app\utils\PermissionHelper;
use yii\helpers\ArrayHelper;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$mailer = require __DIR__ . '/mailer.php';
$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'ru-RU',
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'ePNsOtItiW0Q5cB70CM5oYz0gYz2k224',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'amnah\yii2\user\components\User',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => $mailer,
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => '/site/index',
                '<controller:[\wd-]+>/<id:\d+>' => '<controller>/view',
                '<controller:[\wd-]+>/<action:[\wd-]+>/' => '<controller>/<action>',
                '<controller:[\wd-]+>/<action:[\wd-]+>' => '<controller>/<action>',
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['edit_news']
        ],
        'i18n' => [
            'translations' => [
                'custom' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@vendor/amnah/yii2-user/views' => '@app/views',
                ],
            ],
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'amnah\yii2\user\Module',
            'modelClasses' => [
                'User' => 'app\models\User',
            ],
            'controllerMap' => [
                'admin' => 'app\controllers\AdminController',
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    $dev_config = require __DIR__ . '/dev-web.php';
    $config = ArrayHelper::merge($config, $dev_config);
} elseif (YII_ENV_PROD) {
    $prod_config = require __DIR__ . '/prod-web.php';
    $config = ArrayHelper::merge($config, $prod_config);
}

return $config;
