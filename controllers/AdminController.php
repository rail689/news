<?php

namespace app\controllers;

use amnah\yii2\user\models\Profile;
use app\models\User;
use app\utils\EmailHelper;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class AdminController extends \amnah\yii2\user\controllers\AdminController
{

    public function actionCreate()
    {
        /** @var User $user */
        /** @var Profile $profile */

        $user = new User();
        $user->setScenario(User::SCENARIO_ADMIN);
        $profile = new Profile();

        $post = Yii::$app->request->post();
        $userLoaded = $user->load($post);
        $profile->load($post);

        if ($userLoaded && $user->validate() && $profile->validate()) {
            if ($user->save(false)
                && $profile->setUser($user->id)->save(false)) {
                EmailHelper::sendEmailConfirmation($user);
                return $this->renderAjax('@app/views/layouts/ajax_result', [
                    'message' => Yii::t('custom', 'User save')
                ]);
            }
            return $this->renderAjax('@app/views/layouts/ajax_result', [
                'message' => Yii::t('custom', 'User not save')
            ]);
        }

        // render
        return $this->renderAjax('create', compact('user', 'profile'));
    }

    public function actionUpdate($id)
    {
        $user = $this::findModel($id);
        $user->setScenario(User::SCENARIO_ADMIN);
        $profile = $user->profile;

        $post = Yii::$app->request->post();
        $userLoaded = $user->load($post);
        $profile->load($post);

        if ($userLoaded && $user->validate() && $profile->validate()) {
            if ($user->save(false)
                && $profile->setUser($user->id)->save(false)) {
                return $this->renderAjax('@app/views/layouts/ajax_result', [
                    'message' => Yii::t('custom', 'User save')
                ]);
            }
            return $this->renderAjax('@app/views/layouts/ajax_result', [
                'message' => Yii::t('custom', 'User not save')
            ]);
        }

        return $this->renderAjax('update', compact('user', 'profile'));
    }

    public function actionValidate($id = null)
    {
        if (is_null($id)) {
            $user = new User();
            $profile = new Profile();
        } else {
            $user = $this->findModel($id);
            $profile = $user->profile;
        }
        $user->setScenario(User::SCENARIO_ADMIN);
        $post = Yii::$app->request->post();
        if (Yii::$app->request->isAjax && $user->load($post) && $profile->load($post)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($user, $profile);
        }
    }

    /**
     * @param string $id
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $user = User::findOne($id);
        if ($user) {
            return $user;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

}