<?php

namespace app\controllers;

use app\models\User;
use app\utils\PermissionHelper;
use Yii;
use app\models\News;
use app\models\search\NewsSearch;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'picture'
                        ],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['@']
                    ],
                    [
                        'actions' => [
                            'create',
                            'update',
                            'delete',
                            'validate',
                            'status'
                        ],
                        'roles' => [User::ROLE_MANAGER],
                        'allow' => true,
                    ],
                    [
                        'allow' => false
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!is_null($per_page = Yii::$app->request->get('per-page'))) {
            Yii::$app->session->set('per-page', $per_page);
        }
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();
        $model->profile_id = Yii::$app->user->identity->profile->id;
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if (!is_null($image)){
                $path = $model->namePicture();
            }
            if ($model->save()) {
                if (isset($path)) {
                    $image->saveAs($path);
                }
                return $this->renderAjax('@app/views/layouts/ajax_result', [
                    'message' => Yii::t('custom', 'News save')
                ]);
            }
        }
        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can(PermissionHelper::PERMISSION_EDIT_NEWS, ['news_id' => $id])) {
            throw new ForbiddenHttpException(Yii::t('app', 'Permission dehined'));
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if (!is_null($image)){
                $path = $model->namePicture();
            }
            if ($model->save()) {
                if (isset($path)) {
                    $image->saveAs($path);
                }
                return $this->renderAjax('@app/views/layouts/ajax_result', [
                    'message' => Yii::t('custom', 'News save')
                ]);
            }
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }


    /**
     * @param $path
     * @return $this
     * @throws NotFoundHttpException
     */
    public function actionPicture($name=null)
    {
        if ($content = News::getImage($name)) {
            return Yii::$app->response->sendContentAsFile($content, $name);
        }
        throw new NotFoundHttpException(Yii::t('Error messages', 'File not found'));
    }


    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can(PermissionHelper::PERMISSION_EDIT_NEWS, ['news_id' => $id])) {
            throw new ForbiddenHttpException(Yii::t('app', 'Permission dehined'));
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionStatus($id, $status)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!ArrayHelper::isIn($status, array_keys(News::getStatuses()))) {
            return [
                'status' => 'error',
                'message' => Yii::t('app', 'Status not support')
            ];
        }
        $news = $this->findModel($id);
        if ($news->changeStatus($status)) {
            if ($news->isActive()) {
                $result = [
                    'message' => Yii::t('custom', 'Deactivate'),
                    'status_id' => News::STATUS_INACTIVE
                ];
            } else {
                $result = [
                    'message' => Yii::t('custom', 'Activate'),
                    'status_id' => News::STATUS_ACTIVE
                ];
            }
            return ArrayHelper::merge($result, [
                'status' => 'success',
            ]);
        } else {
            return [
                'status' => 'error',
                'message' => Yii::t('app', 'Status not change')
            ];
        }
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionValidate($id = null)
    {
        if (is_null($id)) {
            $news = new News();
        } else {
            $news = $this->findModel($id);
        }
        $post = Yii::$app->request->post();
        if (Yii::$app->request->isAjax && $news->load($post)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($news);
        }
    }
}
