$(document).on('hidden.bs.modal', '.ajax-modal', function () {
    $(this).removeData('bs.modal');
    $(this).find('.modal-content').html('');
});

$(document).on('click', '.modal-submit', function () {
    var form = $(this).closest('form');
    var method = form.attr('method') || 'post';
    $.ajax({
        url: form.attr('action'),
        type: method,
        data: form.serialize(),
        success: function (response) {
            var e = jQuery.Event("content.beforeReloaded");
            var modal_content = form.parents('.modal-content');
            modal_content.trigger(e);
            if (modal_content.size() > 0) {
                modal_content.html(response);
            }
        },
    });
    return false;
});

$(document).on('click', '.file-modal-submit', function () {
    var formObject = $(this).closest('form');
    var form = formObject[0];

    var formData = new FormData(form);

    var xhr = new XMLHttpRequest();
    xhr.open(form.method, form.action);

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                data = xhr.responseText;
                console.log(data);
                var e = jQuery.Event("content.beforeReloaded");
                var modal_content = formObject.parents('.modal-content');
                modal_content.trigger(e);
                if (modal_content.size() > 0) {
                    modal_content.html(data);
                }
            }
        }
    };

    xhr.send(formData);
});

$(document).on('loaded.bs.modal', '#remote_modal', function () {
    if (!$.trim($(this).find('.modal-body').html())) {
        $('#remote_modal').modal('hide');
    }
});
$(document).ajaxError(function (event, request, settings) {
    if (request.status != 0) {
        alert(request.responseText);
    }
});

$(document).on('click', '.news-status', function () {
    var button = $(this);
    $.ajax({
        url: '/news/status',
        type: 'get',
        data: {'id': button.data('id'), 'status': button.data('status_id')},
        success: function (response) {
            if (response.status == 'success') {
                button.data('status_id', response.status_id);
                button.html(response.message);
            } else {
                alert(response.message);
            }
        },
    });
});