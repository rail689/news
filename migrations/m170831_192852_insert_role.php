<?php

use app\models\Role;
use yii\db\Migration;

/**
 * Class m170831_192852_insert_role
 */
class m170831_192852_insert_role extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%role}}', [
            'name' => 'Manager',
            'id' => Role::ROLE_MANAGER,
            'created_at' => gmdate('Y-m-d H:i:s'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }

}
