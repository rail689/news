<?php

use app\rules\EditNewsRule;
use app\utils\PermissionHelper;
use yii\db\Migration;

/**
 * Class m170902_150029_add_edit_news_rule
 */
class m170902_150029_add_edit_news_rule extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $edit_news_rule = new EditNewsRule();
        $auth->add($edit_news_rule);
        $edit_news_permission = $auth->createPermission(PermissionHelper::PERMISSION_EDIT_NEWS);
        $edit_news_permission->description = 'Просмотр админки';
        $edit_news_permission->ruleName = $edit_news_rule->name;
        $auth->add($edit_news_permission);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $edit_news_rule = new EditNewsRule();
        $edit_news_permission = $auth->createPermission(PermissionHelper::PERMISSION_EDIT_NEWS);
        $auth->remove($edit_news_permission);
        $auth->remove($edit_news_rule);
    }

}
