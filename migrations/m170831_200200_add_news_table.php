<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class m170831_200200_add_news_table
 */
class m170831_200200_add_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%news}}', [
            'id' => Schema::TYPE_PK,
            'status' => Schema::TYPE_SMALLINT . ' not null',
            'full_text' => Schema::TYPE_TEXT,
            'short_text' => Schema::TYPE_TEXT . ' not null',
            'picture_path' => Schema::TYPE_STRING,
            'name' => Schema::TYPE_STRING,
            'profile_id' => Schema::TYPE_INTEGER . ' not null',
            'created_at' => Schema::TYPE_TIMESTAMP . ' null',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' null',
        ]);
        $this->addForeignKey(
            'news_profile_fk',
            '{{%news}}',
            'profile_id',
            '{{%profile}}',
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('news');
    }

}
