<?php

use app\models\User;
use yii\db\Migration;

/**
 * Class m170828_202609_init
 */
class m170828_202609_init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth_manager = Yii::$app->authManager;
        $admin = $auth_manager->createRole(User::ROLE_ADMIN);
        $manager = $auth_manager->createRole(User::ROLE_MANAGER);

        $auth_manager->add($admin);
        $auth_manager->add($manager);


        $auth_manager->addChild($admin, $manager);

        $auth_manager->assign($admin, User::findOne(['username' => 'neo'])->id);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth_manager = Yii::$app->authManager;
        $admin = $auth_manager->createRole(User::ROLE_ADMIN);
        $manager = $auth_manager->createRole(User::ROLE_MANAGER);

        $auth_manager->revoke($admin, User::findOne(['username' => 'neo'])->id);
        $auth_manager->remove($manager);
        $auth_manager->remove($admin);


    }


}
