<?php

namespace app\rules;

use app\models\News;
use app\models\User;
use InvalidArgumentException;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

class EditNewsRule extends Rule
{
    public $name = 'edit_news';

    /**
     * Executes the rule.
     *
     * @param string|int $user the user ID. This should be either an integer or a string representing
     * the unique identifier of a user. See [[\yii\web\User::id]].
     * @param \yii\rbac\Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to [[CheckAccessInterface::checkAccess()]].
     * @return bool a value indicating whether the rule permits the auth item it is associated with.
     * @throws \Exception
     */
    public function execute($user, $item, $params)
    {
        $news_id = ArrayHelper::getValue($params, 'news_id');
        if (empty($news_id)) {
            throw new InvalidArgumentException('news_id must be set');
        }
        if (Yii::$app->user->can(User::ROLE_ADMIN)) {
            return true;
        }
        if (Yii::$app->user->can(User::ROLE_MANAGER)) {
            $news = News::findOne($news_id);
            if (is_null($news)) {
                throw new \Exception('news not found');
            } else {
                if ($news->profile->user_id == $user) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }
}